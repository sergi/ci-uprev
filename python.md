# Python environment

## Python using virtualenv

Based on python 3.11, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 23.0.1 from /usr/lib/python3/dist-packages/pip (python 3.11)
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m venv ci-uprev.venv
$ source ci-uprev.venv/bin/activate
```

To reproduce the same virtual environment that this tools is being tested:

```commandline
$ pip install -r requirements.txt
$ pip install -e .
```

To update the packages in the requirements:

```commandline
$ pip install --upgrade pip
$ pip install colorlog python-gitlab gitpython ruamel.yaml tenacity
$ pip list
Package            Version
------------------ ---------
certifi            2025.1.31
charset-normalizer 3.4.1
colorlog           6.9.0
gitdb              4.0.12
GitPython          3.1.44
idna               3.10
pip                25.0.1
python-gitlab      5.6.0
requests           2.32.3
requests-toolbelt  1.0.0
ruamel.yaml        0.18.10
ruamel.yaml.clib   0.2.12
setuptools         66.1.1
smmap              5.0.2
tenacity           9.0.0
urllib3            2.3.0
$ pip freeze --all > requirements.txt
$ pip install -e .
```

### running tests

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements-test.txt
$ pip install -e .
```

or to the previous `requirements.txt` append:

```commandline
$ python -m pip install pytest pytest-cov responses mypy types-requests
$ pip freeze | grep -E "pytest|pytest-cov|responses|mypy|types-requests"
mypy==1.15.0
mypy-extensions==1.0.0
pytest==8.3.4
pytest-cov==6.0.0
responses==0.25.6
types-requests==2.32.0.20241016
```

The `requirements-test.txt` includes the `requirements.txt` with those packages listed before.

One can use a local repo of `python-gitlab-mock` but its development is deprecated,
and we must migrate this to the same testing pattern than `python-gitlab`.

At this point, one can call pytest:

```commandline
$ pytest
```

### running lint

#### format: black & isort

With the packages `black` and `isort` installed, one can call the script 
`./tools/format.py check` to have a report of what should be changed in the 
formatting. There is a job in the CI of `ci-uprev` that will check this. Check 
the `format-black` job definition in [lint.yml](.gitlab-ci/lint.yml).

#### codespell

There is a script that can do a code spell in `./tools/codespell.py`. There is 
a lint job in the CI of this tool: [lint.yml](.gitlab-ci/lint.yml).

#### type checking

To help interpreting what the code does and how the structures are made, 
a type checking has been set up. There is a tool in `./tools/mypy.py` to 
the check locally, and there is a CI job in this project: [lint.yml](.gitlab-ci/lint.yml).

