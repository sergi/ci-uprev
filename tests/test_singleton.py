#!/usr/bin/python3 -B

# Copyright (C) 2025 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2025 Collabora Ltd"


from unittest import TestCase

from uprev.abstract import KeyedSingleton, Singleton


class TestSingleton(TestCase):
    def test_creation(self) -> None:
        bar = Singleton()
        self.assertEqual(id(bar), id(Singleton()))

    def test_delete(self) -> None:
        bar = Singleton()
        foo = Singleton()
        Singleton.singleton_clean()
        not_bar = Singleton()
        self.assertEqual(id(bar), id(foo))
        self.assertNotEqual(id(bar), id(not_bar))


class TestKeyedSingleton(TestCase):
    def test_creation(self) -> None:
        bar = KeyedSingleton("bar")
        foo = KeyedSingleton("foo")
        self.assertEqual(id(bar), id(KeyedSingleton("bar")))
        self.assertEqual(id(foo), id(KeyedSingleton("foo")))
        self.assertIsNotNone(KeyedSingleton._instances)
        if KeyedSingleton._instances is not None:
            self.assertEqual(list(KeyedSingleton._instances.keys()), ["bar", "foo"])

    def test_delete(self) -> None:
        with self.assertRaises(KeyError):
            KeyedSingleton.singleton_clean("barfoo")
        KeyedSingleton("bar")
        KeyedSingleton("foo")
        self.assertIsNotNone(KeyedSingleton._instances)
        if KeyedSingleton._instances is not None:
            self.assertEqual(list(KeyedSingleton._instances.keys()), ["bar", "foo"])
            KeyedSingleton.singleton_clean("bar")
            self.assertEqual(list(KeyedSingleton._instances.keys()), ["foo"])
