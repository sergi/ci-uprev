#!/usr/bin/python3 -B

# Copyright (C) 2025 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2025 Collabora Ltd"

import sys
from logging import DEBUG, StreamHandler, basicConfig, getLogger
from os import environ
from pathlib import Path

from responses import _recorder
from test_environment import (
    BRANCH_RECORD_FILE,
    DEPENDENCY_RECORD_FILE,
    FORK_PATH_WITH_NAMESPACE,
    FORK_RECORD_FILE,
    GET_TARGET_RECORD_FILE,
    GITLAB_RECORD_FILE,
    LOCALCLONE_RECORD_FILE,
    MESA_PATH_WITH_NAMESPACE,
    PIGLIT_PATH_WITH_NAMESPACE,
    TARGET_NAMESPACE,
    TARGET_PROJECT,
    TARGET_RECORD_FILE,
    TEMPLATE_RECORD_FILE,
    TITLE_RECORD_FILE,
    UPDATEREVISIONPAIR_RECORD_FILE,
    WORKING_RECORD_FILE,
    clean_branch_singleton,
    clean_dependency_singleton,
    clean_fork_singleton,
    clean_get_target_singleton,
    clean_gitlabproxy_singleton,
    clean_localclone_singleton,
    clean_target_singleton,
    clean_template_singleton,
    clean_title_singleton,
    clean_updaterevisionpair_singleton,
    clean_working_singleton,
    request_branch_singleton,
    request_dependency_singleton,
    request_fork_singleton,
    request_get_target_singleton,
    request_gitlabproxy_singleton,
    request_localclone_singleton,
    request_target_singleton,
    request_template_singleton,
    request_title_singleton,
    request_uprevrevisionpair_singleton,
    request_working_singleton,
)

logger = getLogger(__name__)


def prepare_recording() -> None:
    __setup_logging()


def record_environment() -> None:
    record_gitlabproxy_singleton()
    record_target_singleton()
    record_fork_singleton()
    record_get_target_singleton()
    record_dependency_singleton()
    record_working_singleton()
    record_template_singleton()
    record_updaterevisionpair_singleton()
    record_localclone_singleton()
    record_branch_singleton()
    record_title_singleton()


def record_gitlabproxy_singleton() -> None:
    logger.debug("======== recorder_build_gitlab_proxy ========")
    clean_gitlabproxy_singleton()
    recorder_gitlabproxy_singleton()
    post_process_yaml(GITLAB_RECORD_FILE)


@_recorder.record(file_path=GITLAB_RECORD_FILE)  # type: ignore [misc]
def recorder_gitlabproxy_singleton() -> None:
    request_gitlabproxy_singleton()


def record_target_singleton() -> None:
    logger.debug("======== recorder_build_target ========")
    clean_target_singleton()
    recorder_target_singleton()
    post_process_yaml(TARGET_RECORD_FILE)


@_recorder.record(file_path=TARGET_RECORD_FILE)  # type: ignore [misc]
def recorder_target_singleton() -> None:
    environ["TARGET_PROJECT_PATH"] = f"{TARGET_NAMESPACE}/{TARGET_PROJECT}"
    request_target_singleton()


def record_fork_singleton() -> None:
    logger.debug("======== recorder_build_fork ========")
    clean_fork_singleton()
    recorder_fork_singleton()
    post_process_yaml(FORK_RECORD_FILE)


@_recorder.record(file_path=FORK_RECORD_FILE)  # type: ignore [misc]
def recorder_fork_singleton() -> None:
    environ["FORK_PROJECT_PATH"] = FORK_PATH_WITH_NAMESPACE
    request_fork_singleton()


def record_get_target_singleton() -> None:
    logger.debug("======== recorder_get_target ========")
    clean_get_target_singleton()
    recorder_get_target()
    post_process_yaml(GET_TARGET_RECORD_FILE)


@_recorder.record(file_path=GET_TARGET_RECORD_FILE)  # type: ignore [misc]
def recorder_get_target() -> None:
    environ["PRODUCTION"] = "true"
    environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
    request_get_target_singleton()


def record_dependency_singleton() -> None:
    logger.debug("======== recorder_build_dependency ========")
    clean_dependency_singleton()
    recorder_dependency_singleton()
    post_process_yaml(DEPENDENCY_RECORD_FILE)


@_recorder.record(file_path=DEPENDENCY_RECORD_FILE)  # type: ignore [misc]
def recorder_dependency_singleton() -> None:
    environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE
    request_dependency_singleton()


def record_working_singleton() -> None:
    logger.debug("======== recorder_build_working ========")
    clean_working_singleton()
    recorder_working_singleton()
    post_process_yaml(WORKING_RECORD_FILE)


@_recorder.record(file_path=WORKING_RECORD_FILE)  # type: ignore [misc]
def recorder_working_singleton() -> None:
    environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
    request_working_singleton()


def record_template_singleton() -> None:
    logger.debug("======== recorder_build_template ========")
    clean_template_singleton()
    recorder_template_singleton()
    post_process_yaml(TEMPLATE_RECORD_FILE)


@_recorder.record(file_path=TEMPLATE_RECORD_FILE)  # type: ignore [misc]
def recorder_template_singleton() -> None:
    environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
    request_template_singleton()


def record_updaterevisionpair_singleton() -> None:
    logger.debug("======== recorder_updaterevisionpair ========")
    clean_updaterevisionpair_singleton()
    recorder_updaterevisionpair_singleton()
    post_process_yaml(UPDATEREVISIONPAIR_RECORD_FILE)


@_recorder.record(file_path=UPDATEREVISIONPAIR_RECORD_FILE)  # type: ignore [misc]
def recorder_updaterevisionpair_singleton() -> None:
    environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
    environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE
    request_uprevrevisionpair_singleton()


def record_localclone_singleton() -> None:
    logger.debug("======== recorder_localclone ========")
    clean_localclone_singleton()
    recorder_localclone_singleton()
    post_process_yaml(LOCALCLONE_RECORD_FILE)


@_recorder.record(file_path=LOCALCLONE_RECORD_FILE)  # type: ignore [misc]
def recorder_localclone_singleton() -> None:
    environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
    request_localclone_singleton()


def record_branch_singleton() -> None:
    logger.debug("======== recorder_branch ========")
    clean_branch_singleton()
    recorder_branch_singleton()
    post_process_yaml(BRANCH_RECORD_FILE)


@_recorder.record(file_path=BRANCH_RECORD_FILE)  # type: ignore [misc]
def recorder_branch_singleton() -> None:
    environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
    environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE
    environ["PRODUCTION"] = "true"
    request_branch_singleton()


def record_title_singleton() -> None:
    logger.debug("======== recorder_title ========")
    clean_title_singleton()
    recorder_title_singleton()
    post_process_yaml(TITLE_RECORD_FILE)


@_recorder.record(file_path=TITLE_RECORD_FILE)  # type: ignore [misc]
def recorder_title_singleton() -> None:
    environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE
    environ["PRODUCTION"] = "true"
    request_title_singleton()


def __setup_logging() -> None:
    basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=DEBUG)
    console_handler = StreamHandler(stream=sys.stdout)
    console_handler.level = DEBUG


def post_process_yaml(file_name: str) -> None:
    file = Path(file_name)
    file.write_text(
        file.read_text().replace(
            "content_type: text/plain", "content_type: application/json"
        )
    )


def main() -> None:
    prepare_recording()
    record_environment()


if __name__ == "__main__":
    main()
