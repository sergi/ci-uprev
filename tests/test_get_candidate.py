#!/usr/bin/python3 -B

# Copyright (C) 2025 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2025 Collabora Ltd"

import json
from logging import getLogger
from os import environ
from pathlib import Path
from typing import Any
from unittest import TestCase

import responses
from common import clean_singletons

from uprev.environment import Dependency, GitlabProxy, Target, UpdateRevisionPair
from uprev.main import get_candidate_for_uprev

logger = getLogger(__name__)

TEST_ROOT = Path(__file__).parent / "get_candidate"
RECORD_FILE = "test_get_candidate_for_uprev%s.yaml"
GET_THE_LAST = str(TEST_ROOT / str(RECORD_FILE % ""))
GET_FROM_REVISION = str(TEST_ROOT / str(RECORD_FILE % "_from_revision"))
GET_FROM_PIPELINE_ID = str(TEST_ROOT / str(RECORD_FILE % "_from_pipeline"))
GET_FROM_BOTH = str(TEST_ROOT / str(RECORD_FILE % "_from_both"))
GET_WRONG_PAIR = str(TEST_ROOT / str(RECORD_FILE % "_wrong_pair"))
JSON_FILE = str(TEST_ROOT / "get_candidate.json")
TARGET_PROJECT_PATH = "virgl/virglrenderer"
DEP_PROJECT_PATH = "mesa/mesa"
UNRELATED_PIPELINE_ID = 1234


class TestGetCandidateForUprev(TestCase):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super(TestGetCandidateForUprev, self).__init__(*args, **kwargs)
        with open(JSON_FILE, mode="r", encoding="utf-8") as read_file:
            get_candidate = json.load(read_file)
            self._revision = get_candidate["revision"]
            self._pipeline_id = get_candidate["pipeline_id"]
        logger.info(
            "Testing uprev %s in %s, dependency revision %s and pipeline id %d",
            DEP_PROJECT_PATH,
            TARGET_PROJECT_PATH,
            self._revision,
            self._pipeline_id,
        )

    def __setup_environment(self) -> None:
        environ["TARGET_PROJECT_PATH"] = TARGET_PROJECT_PATH
        environ["DEP_PROJECT_PATH"] = DEP_PROJECT_PATH

    def test_get_candidate_latest(self) -> None:
        clean_get_candidate_latest()
        self.__setup_environment()

        logger.info("Responses coming from %s", GET_THE_LAST)
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(GET_THE_LAST)

            revision, pipeline_id = request_get_candidate_latest()

        self.assertEqual(revision, self._revision)
        self.assertEqual(pipeline_id, self._pipeline_id)

    def test_get_candidate_from_revision(self) -> None:
        clean_get_candidate_from_revision()
        self.__setup_environment()

        logger.info("Responses coming from %s}", GET_FROM_REVISION)
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(GET_FROM_REVISION)

            revision, pipeline_id = request_get_candidate_from_revision(self._revision)

        self.assertEqual(revision, self._revision)
        self.assertEqual(pipeline_id, self._pipeline_id)

    def test_get_candidate_from_pipeline(self) -> None:
        clean_get_candidate_from_pipeline()
        self.__setup_environment()

        logger.info("Responses coming from %s", GET_FROM_PIPELINE_ID)
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(GET_FROM_PIPELINE_ID)

            revision, pipeline_id = request_get_candidate_from_pipeline(
                self._pipeline_id
            )

        self.assertEqual(revision, self._revision)
        self.assertEqual(pipeline_id, self._pipeline_id)

    def test_get_candidate_from_both(self) -> None:
        clean_get_candidate_from_both()
        self.__setup_environment()

        logger.info("Responses coming from %s", GET_FROM_BOTH)
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(GET_FROM_BOTH)

            revision, pipeline_id = request_get_candidate_from_both(
                self._revision, self._pipeline_id
            )

        self.assertEqual(revision, self._revision)
        self.assertEqual(pipeline_id, self._pipeline_id)

    def test_get_candidate_from_wrong_pair(self) -> None:
        clean_get_candidate_from_wrong_pair()
        self.__setup_environment()

        logger.info("Responses coming from %s", GET_WRONG_PAIR)
        with self.assertRaises(AssertionError) as exc:
            with responses.RequestsMock() as rsps:
                rsps._add_from_file(GET_WRONG_PAIR)

                request_get_candidate_from_wrong_pair(self._revision)

        expected_exception = exc.expected
        expected_exception_name = expected_exception.__name__  # type: ignore
        exception_itself = exc.exception
        logger.info(
            "Correctly raised %s: '%s'", expected_exception_name, exception_itself
        )


def clean_get_candidate_latest() -> None:
    clean_singletons([GitlabProxy, Dependency, UpdateRevisionPair, Target])


def request_get_candidate_latest() -> tuple[str, int]:
    return get_candidate_for_uprev(None, None)


def clean_get_candidate_from_revision() -> None:
    clean_singletons([GitlabProxy, Dependency])


def request_get_candidate_from_revision(revision: str) -> tuple[str, int]:
    return get_candidate_for_uprev(revision, None)


def clean_get_candidate_from_pipeline() -> None:
    clean_singletons([GitlabProxy, Dependency])


def request_get_candidate_from_pipeline(pipeline_id: int) -> tuple[str, int]:
    return get_candidate_for_uprev(None, pipeline_id)


def clean_get_candidate_from_both() -> None:
    clean_singletons([GitlabProxy, Dependency])


def request_get_candidate_from_both(revision: str, pipeline_id: int) -> tuple[str, int]:
    return get_candidate_for_uprev(revision, pipeline_id)


def clean_get_candidate_from_wrong_pair() -> None:
    clean_singletons([GitlabProxy, Dependency])


def request_get_candidate_from_wrong_pair(revision: str) -> tuple[str, int]:
    return get_candidate_for_uprev(revision, UNRELATED_PIPELINE_ID)
