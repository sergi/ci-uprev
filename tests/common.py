#!/usr/bin/env python3.11

# Copyright (C) 2025 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2025 Collabora Ltd"

from typing import Any

from uprev.abstract import KeyedSingleton, Singleton


def clean_singletons(singletons: list[Any]) -> None:
    """
    From a list of singleton subclasses, call their singleton_clean method.
    For KeyedSingleton
    :param singletons:
    :return:
    """
    for singleton_cls in singletons:
        if isinstance(singleton_cls, type(Singleton)):
            singleton_cls.singleton_clean()
            continue
        elif isinstance(singleton_cls, type(KeyedSingleton)):
            if isinstance(singleton_cls._instances, dict):
                for key in list(singleton_cls._instances.keys()):
                    singleton_cls.singleton_clean(key)
        elif isinstance(singleton_cls, tuple) and len(singleton_cls) == 2:
            keyed_singleton, key = singleton_cls
            is_keyed_singleton = isinstance(keyed_singleton, type(KeyedSingleton))
            comes_with_a_key = isinstance(key, str)
            if is_keyed_singleton and comes_with_a_key:
                keyed_singleton.singleton_clean(key)
                continue
        raise AssertionError(f"Unable to clean singleton for {singleton_cls.__name__}")
