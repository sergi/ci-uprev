-r requirements.txt
mypy==1.15.0
mypy-extensions==1.0.0
pytest==8.3.4
pytest-cov==6.0.0
responses==0.25.6
types-requests==2.32.0.20241016
