#!/usr/bin/python3 -B

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from argparse import ArgumentParser
from functools import partial
from pathlib import Path
from subprocess import run as _run

PROJECT_ROOT = Path(__file__).parent.parent
PYTHON_FILES = (
    PROJECT_ROOT / "uprev/",
    PROJECT_ROOT / "tests/",
    PROJECT_ROOT / "tools/",
    PROJECT_ROOT / "setup.py",
)

run = partial(_run, check=True, cwd=PROJECT_ROOT)
ISORT_BASE_ARGS = ("isort", "--verbose", "--only-modified", "--profile", "black")
BLACK_BASE_ARGS = ("black",)
CHECK_ARGS = ("--check", "--diff")


def run_format() -> None:
    run(ISORT_BASE_ARGS + PYTHON_FILES)
    run(BLACK_BASE_ARGS + PYTHON_FILES)


def run_check() -> None:
    run(ISORT_BASE_ARGS + CHECK_ARGS + PYTHON_FILES)
    run(BLACK_BASE_ARGS + CHECK_ARGS + PYTHON_FILES)


STAGES = {
    "format": run_format,
    "check": run_check,
}


def run_stage(stage_name: str) -> None:
    STAGES[stage_name]()


def main() -> None:
    parser = ArgumentParser("format")
    parser.add_argument(
        "stage_name",
        choices=STAGES.keys(),
    )

    run_stage(**vars(parser.parse_args()))


if __name__ == "__main__":
    main()
