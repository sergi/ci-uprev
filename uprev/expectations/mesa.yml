---
# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

# TODO: this information may extracted from the `.gitlab-ci.yml` merged file
#  - The 'path' seems to be where the .gitlab-ci.yml defining the job is
#  - The 'files' have a chain of variables to collect it, often `GPU_VERSION`

# === amd stage ===
# --- src/amd/ci/gitlab-ci.yml ---
amd-raven-skqp:
  path: "src/amd/ci"
  files: "amd-raven"
glcts-vangogh-valve:
  path: "src/amd/ci"
  files: "radeonsi-vangogh"
radeonsi-raven-cdna-lower-image:
  path: "src/amd/ci"
  files: "radeonsi-raven-cdna"
radeonsi-raven-piglit:
  path: "src/amd/ci"
  files: "radeonsi-raven"
radeonsi-raven-va:
  path: "src/amd/ci"
  files: "radeonsi-raven"
radeonsi-stoney-gl:
  path: "src/amd/ci"
  files: "radeonsi-stoney"
radv-raven-vkcts:
  path: "src/amd/ci"
  files: "radv-raven"
radv-stoney-angle:
  path: "src/amd/ci"
  files: "angle-radv-stoney-aco"
radv-stoney-vkcts:
  path: "src/amd/ci"
  files: "radv-stoney-aco"
vkcts-hawaii-valve:
  path: "src/amd/ci"
  files: "radv-hawaii-aco"
vkcts-kabini-valve:
  path: "src/amd/ci"
  files: "radv-kabini-aco"
vkcts-navi10-valve:
  path: "src/amd/ci"
  files: "radv-navi10-aco"
vkcts-navi21-llvm-valve:
  path: "src/amd/ci"
  files: "radv-navi21-llvm"
vkcts-navi21-valve:
  path: "src/amd/ci"
  files: "radv-navi21-aco"
vkcts-navi31-valve:
  path: "src/amd/ci"
  files: "radv-navi31-aco"
vkcts-polaris10-valve:
  path: "src/amd/ci"
  files: "radv-polaris10-aco"
vkcts-raphael-valve:
  path: "src/amd/ci"
  files: "radv-raphael-aco"
vkcts-renoir-valve:
  path: "src/amd/ci"
  files: "radv-renoir-aco"
vkcts-stoney-valve:
  path: "src/amd/ci"
  files: "radv-stoney-aco"
vkcts-tahiti-valve:
  path: "src/amd/ci"
  files: "radv-tahiti-aco"
vkcts-vangogh-valve:
  path: "src/amd/ci"
  files: "radv-vangogh-aco"
vkcts-vega10-valve:
  path: "src/amd/ci"
  files: "radv-vega10-aco"
# --- src/gallium/drivers/r300/ci/gitlab-ci.yml ---
r300-rv380-deqp-gles2:
  path: "src/gallium/drivers/r300/ci"
  files: "r300-rv380"
r300-rv410-deqp-piglit:
  path: "src/gallium/drivers/r300/ci"
  files: "r300-rv410"
r300-rv530-deqp-gles2:
  path: "src/gallium/drivers/r300/ci"
  files: "r300-rv530-nohiz"
r300-rv530-nine:
  path: "src/gallium/drivers/r300/ci"
  files: "r300-rv530-nohiz"
r300-rv530-piglit:
  path: "src/gallium/drivers/r300/ci"
  files: "r300-rv530-nohiz"
# === end amd stage ===

# === arm stage ===
# --- src/panfrost/ci/gitlab-ci.yml ---
panfrost-g52-gl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g52"
panfrost-g52-piglit:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g52"
panfrost-g52-piglit-gles2:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g52"
panfrost-g52-vk:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g52"
panfrost-g610-gl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g610"
panfrost-g610-piglit:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g610"
panfrost-g610-vk:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g610"
panfrost-g57-gl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g57"
panfrost-g57-piglit:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g57"
panfrost-g72-gl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-g72"
panfrost-t720-gles2:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-t720"
panfrost-t760-gles:arm32:
  path: "src/panfrost/ci"
  files: "panfrost-t760"
panfrost-t860-cl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-t860"
panfrost-t860-egl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-t860"
panfrost-t860-gl:arm64:
  path: "src/panfrost/ci"
  files: "panfrost-t860"
# --- src/gallium/drivers/lima/ci/gitlab-ci.yml ---
lima-mali450-deqp:arm64:
  path: "src/gallium/drivers/lima/ci"
  files: "lima"
lima-mali450-piglit:arm64:
  path: "src/gallium/drivers/lima/ci"
  files: "lima"
# === end arm stage ===

# === broadcom stage ===
# --- src/broadcom/ci/gitlab-ci.yml ---
vc4-rpi3-gl:arm32:
  path: "src/broadcom/ci"
  files: "broadcom-rpi3"
vc4-rpi3-gl-asan:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi3"
vc4-rpi3-gl-ubsan:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi3"
vc4-rpi3-gl:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi3"
vc4-rpi3-gl-piglit:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi3"
v3d-rpi4-gl:arm32:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3d-rpi4-gl:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3d-rpi4-gl-asan:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3d-rpi4-gl-ubsan:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3d-rpi4-rusticl:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3dv-rpi4-vk:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3dv-rpi4-vk-asan:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3dv-rpi4-vk-ubsan:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi4"
v3d-rpi5-gl:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi5"
v3d-rpi5-rusticl:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi5"
v3dv-rpi5-vk:arm64:
  path: "src/broadcom/ci"
  files: "broadcom-rpi5"
# === end broadcom stage ===

# === etnaviv stage ===
# --- src/etnaviv/ci/gitlab-ci.yml ---
gc2000-gles2:
  path: "src/etnaviv/ci"
  files: "etnaviv-gc2000-r5108"
gc2000-piglit:
  path: "src/etnaviv/ci"
  files: "etnaviv-gc2000-r5108"
gc3000-gles2:
  path: "src/etnaviv/ci"
  files: "etnaviv-gc3000-r5450"
gc7000-gles2:
  path: "src/etnaviv/ci"
  files: "etnaviv-gc7000-r6214"
# === end etnaviv stage ===

# === freedreno stage ===
# --- src/freedreno/ci/gitlab-ci.yml ---
a306-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a306"
a306-piglit:
  path: "src/freedreno/ci"
  files: "freedreno-a306"
a306-piglit-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a306"
a306-piglit-shader:
  path: "src/freedreno/ci"
  files: "freedreno-a630"
a530-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a530"
a530-piglit:
  path: "src/freedreno/ci"
  files: "freedreno-a530"
a618-angle:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a618-egl:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a618-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a618-piglit:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a618-piglit-cl:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a618-skqp:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a618-vk:
  path: "src/freedreno/ci"
  files: "freedreno-a618"
a630-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a630"
a630-gles-asan:
  path: "src/freedreno/ci"
  files: "freedreno-a630-asan"
a630-piglit:
  path: "src/freedreno/ci"
  files: "freedreno-a630"
a630-vk:
  path: "src/freedreno/ci"
  files: "freedreno-a630"
a630-vk-asan:
  path: "src/freedreno/ci"
  files: "freedreno-a630"
a660-angle:
  path: "src/freedreno/ci"
  files: "freedreno-a660"
a660-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a660"
a660-piglit-cl:
  path: "src/freedreno/ci"
  files: "freedreno-a660"
a660-vk:
  path: "src/freedreno/ci"
  files: "freedreno-a660"
a750-angle:
  path: "src/freedreno/ci"
  files: "freedreno-a750"
a750-gl:
  path: "src/freedreno/ci"
  files: "freedreno-a750"
a750-piglit-cl:
  path: "src/freedreno/ci"
  files: "freedreno-a750"
a750-vk:
  path: "src/freedreno/ci"
  files: "freedreno-a750"
a750-vkd3d:
  path: "src/freedreno/ci"
  files: "freedreno-a750-vkd3d"
# === end freedreno stage ===

# === intel stage ==
# --- src/intel/ci/gitlab-ci.yml ---
anv-adl-vk:
  path: "src/intel/ci"
  files: "anv-adl"
anv-adl-angle:
  path: "src/intel/ci"
  files: "angle-anv-adl"
anv-jsl-vk:
  path: "src/intel/ci"
  files: "anv-jsl"
anv-jsl-angle:
  path: "src/intel/ci"
  files: "angle-anv-jsl"
anv-tgl-vk:
  path: "src/intel/ci"
  files: "anv-tgl"
anv-tgl-angle:
  path: "src/intel/ci"
  files: "angle-anv-tgl"
hasvk-hsw:
  path: "src/intel/ci"
  files: "hasvk-hsw"
intel-adl-cl:
  path: "src/intel/ci"
  files: "intel-adl"
intel-adl-skqp:
  path: "src/intel/ci"
  files: "intel-adl"
intel-tgl-skqp:
  path: "src/intel/ci"
  files: "intel-tgl"
iris-amly-deqp:
  path: "src/intel/ci"
  files: "iris-amly"
iris-amly-egl:
  path: "src/intel/ci"
  files: "iris-amly"
iris-apl-deqp:
  path: "src/intel/ci"
  files: "iris-apl"
iris-apl-egl:
  path: "src/intel/ci"
  files: "iris-apl"
iris-cml-deqp:
  path: "src/intel/ci"
  files: "iris-cml"
iris-glk-deqp:
  path: "src/intel/ci"
  files: "iris-glk"
iris-glk-egl:
  path: "src/intel/ci"
  files: "iris-glk"
iris-jsl-deqp:
  path: "src/intel/ci"
  files: "iris-jsl"
iris-kbl-deqp:
  path: "src/intel/ci"
  files: "iris-kbl"
iris-kbl-piglit:
  path: "src/intel/ci"
  files: "iris-kbl"
# --- src/gallium/drivers/i915/ci/gitlab-ci.yml ---
i915-g33:
  path: "src/gallium/drivers/i915/ci"
  files: "i915-g33"
# === end intel stage ===

# === layered-backends stage ===
# --- src/gallium/drivers/d3d12/ci/gitlab-ci.yml ---
test-d3d12-quick_gl:
  path: "src/gallium/drivers/d3d12/ci"
  files: "d3d12-quick_gl"
test-d3d12-quick_shader:
  path: "src/gallium/drivers/d3d12/ci"
  files: "d3d12-quick_shader"
# --- src/gallium/drivers/svga/ci/gitlab-ci.yml ---
vmware-vmx-piglit:x86_64:
  path: "src/gallium/drivers/svga/ci"
  files: "svga"
# --- src/gallium/drivers/virgl/ci/gitlab-ci.yml ---
android-virgl-llvmpipe:
  path: "src/gallium/drivers/virgl/ci"
  files: "virgl-gl"
virgl-on-gl:
  path: "src/gallium/drivers/virgl/ci"
  files: "virgl-gl"
virgl-on-gles:
  path: "src/gallium/drivers/virgl/ci"
  files: "virgl-gles"
virpipe-on-gl:
  path: "src/gallium/drivers/virgl/ci"
  files: "virpipe-gl"
# --- src/gallium/drivers/zink/ci/gitlab-ci.yml ---
zink-anv-adl:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-anv-adl"
zink-anv-tgl:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-anv-tgl"
zink-lvp:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-lvp"
zink-nvk-ga106-valve:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-nvk-ga106"
zink-radv-polaris10-valve:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-radv-polaris10"
zink-radv-navi10-valve:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-radv-navi10"
zink-radv-navi31-valve:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-radv-navi31"
zink-radv-vangogh-valve:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-radv-vangogh"
zink-tu-a618:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-tu-a618"
zink-tu-a750:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-tu-a750"
zink-venus-lvp:
  path: "src/gallium/drivers/zink/ci"
  files: "zink-venus-lvp"
# --- src/microsoft/ci/gitlab-ci.yml ---
test-dozen-deqp:
  path: "src/microsoft/ci"
  files: "warp"
# --- src/virtio/ci/gitlab-ci.yml ---
venus-lavapipe:
  path: "src/virtio/ci"
  files: "venus"
# === end layered-backends stage ===

# === nouveau stage ===
# --- src/gallium/drivers/nouveau/ci/gitlab-ci.yml ---
gk20a-gles:
  path: "src/gallium/drivers/nouveau/ci"
  files: "nouveau-gk20a"
gm20b-gles:
  path: "src/gallium/drivers/nouveau/ci"
  files: "nouveau-gm20b"
# --- src/nouveau/ci/gitlab-ci.yml ---
nvk-ga106-vkcts-valve:
  path: "src/nouveau/ci"
  files: "nvk-ga106"
nvk-ga106-vkd3d-valve:
  path: "src/nouveau/ci"
  files: "nvk-ga106-vkd3d"
# === end nouveau stage ===

# === software-renderer stage ===
# --- src/gallium/drivers/llvmpipe/ci/gitlab-ci.yml ---
llvmpipe:
  path: "src/gallium/drivers/llvmpipe/ci"
  files: "llvmpipe"
  # FIXME: also found a failing job with results in src/gallium/drivers/svga/ci/llvmpipe-fails.txt
llvmpipe-deqp-asan:
  path: "src/gallium/drivers/llvmpipe/ci"
  files: "llvmpipe-asan"
llvmpipe-piglit-rusticl:
  path: "src/gallium/drivers/llvmpipe/ci"
  files: "llvmpipe-rusticl"
# --- src/gallium/drivers/softpipe/ci/gitlab-ci.yml ---
softpipe:
  path: "src/gallium/drivers/softpipe/ci"
  files: "softpipe"
softpipe-asan-gles31:
  path: "src/gallium/drivers/softpipe/ci"
  files: "softpipe-asan"
# --- src/gallium/drivers/svga/ci/gitlab-ci.yml ---
# --- src/gallium/frontends/lavapipe/ci/gitlab-ci.yml ---
android-angle-lavapipe:
  path: "src/gallium/frontends/lavapipe/ci"
  files: "lvp-android-angle"
lavapipe:
  path: "src/gallium/frontends/lavapipe/ci"
  files: "lvp"
lavapipe-vk-asan:
  path: "src/gallium/frontends/lavapipe/ci"
  files: "lvp-asan"
lavapipe-vkd3d:
  path: "src/gallium/frontends/lavapipe/ci"
  files: "lvp-vkd3d"
# === end software-renderer stage ===

